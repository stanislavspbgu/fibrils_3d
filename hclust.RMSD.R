library(fpc)
library(dendextend)
library(ggdendro)
library(ggplot2)

df = read.csv('~/Documents/bioinf/ScienceProject/fibrils_3d/fit_res_17.csv')
df = df[with(df, order(ID1, ID2)), ]
head(df)
str(df)

M_RMSD = matrix(as.numeric(df$RMSD), ncol = length(unique(df$ID1)), nrow = length(unique(df$ID2)))
# sum(M_RMSD)

# for(k in 1:n){
    # i = df[k,1]
    # j = df[k,2]
    # M_RMSD[i,j] = df[k,3]
# }

M_RMSD[1,1]

dist_RMSD = as.dist(M_RMSD)
Clust = hclust(d = dist_RMSD, method='ward.D')
plot(Clust)

angles <- read.csv('~/Documents/bioinf/ScienceProject/fibrils_3d/structs.csv')

str(df)
str(angles)

str(Clust)

LABS <- colnames(M_RMSD)[Clust$order]
LABS <- order(unique(df$ID1))[Clust$order]
str(data.frame(LABS))
tmp2 <- merge(data.frame(LABS), angles, by.x = 'LABS', by.y='ID')
tmp2
groupCodes <- tmp2$Type
groupCodes
length(unique(groupCodes))
colorCodes <- c(
    '#f71905', # 1
    '#02caf7' # 2
    #'#f3f702', # 3
    #'#68f702', # 4
    #'#02f7b2', # 5
    #'#02caf7', # 6
    #'#0278f7', # 7
    #'#0004ff', # 8
    #'#b700ff', # 9
    #'#ff00f7', # 10
    #'#ff009d', # 11
    #'#ff004c', # 12
    #'#4a0303' # 13
)

names(colorCodes) <- unique(tmp2$Type)
colorCodes

dend <- as.dendrogram(Clust)
labels_colors(dend) <- colorCodes[groupCodes][order.dendrogram(dend)]
plot(dend, horiz = T)
