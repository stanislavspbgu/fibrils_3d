import pymol as pm
from findseq import findseq

import sys

def eprint(*args, **kwargs):
        print(*args, file=sys.stderr, **kwargs)

fname = 'pdb/'
ex = '.cif'


# Создание словаря со структурами


with open('for_clust.csv') as file:
    arr = file.read().split('\n')[1:-1]

structs = dict()

for i in range(len(arr)):
    temp = arr[i].split(',')
    structs[int(temp[0])] = {
            'pdb_name':temp[1],
            'chain':temp[2],
            'sequence':temp[3]
            }

# Fitting
problems_resi = []
problems_CA = []

N = 5

some_string = '\\'*10 + '|'*10 + '/'*10 + '-'*10

try:
    m = int(sys.argv[1])
    eprint(f'Starting with m = {m}...')
except:
    m = len(structs)
    eprint(f'Starting with m = {m}...')

with open(f'fit_res_{m}.csv', 'w') as file:
    file.writelines('ID1,ID2,RMSD,D1,D2,D3,D4,D5,D6\n')

for k1 in list(structs.keys())[:m]:
    for k2 in list(structs.keys())[:m]:
        ts = some_string[k2%len(some_string)]
        if (k1*m+k2)%((m**2+m)//m**2) == 0: 
            eprint(f'Filling in the table:{(k1*m + k2)/(m**2 + m)*100:.6f}% {ts}\r', end='')
        if k1 != k2:
            name1 = structs[k1]['pdb_name'].lower()
            name2 = structs[k2]['pdb_name'].lower()
            chain1 = structs[k1]['chain']
            chain2 = structs[k2]['chain']
            seq1 = structs[k1]['sequence']
            seq2 = structs[k2]['sequence']

            if structs[k1]['pdb_name'] != structs[k2]['pdb_name']:
                pm.api.load(fname + name1 + ex, quiet=10)
                pm.api.load(fname + name2 + ex, quiet=10)
            else:
                pm.api.load(fname + name1 + ex, quiet=1)

            # load chains
            pm.api.select('s1', f'{name1}/{chain1}///', quiet=1)
            pm.api.select('s2', f'{name2}/{chain2}///', quiet=1)

            findseq(seq1, 's1', 'seq1')
            findseq(seq2, 's2', 'seq2')

            resi1 = []
            atoms1 = pm.api.get_model('seq1')
            for at in atoms1.atom:
                resi1 += [int(at.resi)]

            resi2 = []
            atoms2 = pm.api.get_model('seq2')
            for at in atoms2.atom:
                resi2 += [int(at.resi)]

            if resi1 == []:
                problems_resi.append([k1, name1, chain1])
                pm.api.delete('all')
                continue
            if resi2 == []:
                problems_resi.append([k2, name2, chain2])
                pm.api.delete('all')
                continue

            min1 = min(resi1)
            max1 = max(resi1)
            min2 = min(resi2)
            max2 = max(resi2)

            int1 = f'{min1}-{min1+2}+{max1-2}-{max1}'
            int2 = f'{min2}-{min2+2}+{max2-2}-{max2}'

            # Выделение CA атомов

            pm.api.select('CA_atoms1', f'seq1///{int1}/CA', quiet=1)
            pm.api.select('CA_atoms2', f'seq2///{int2}/CA', quiet=1)

            # Нахождение расстояний

            ints1 = [min1, min1+1, min1+2, max1-2, max1-1, max1]
            ints2 = [min2, min2+1, min2+2, max2-2, max2-1, max2]

            dists = [0]*6

            #for i in range(6):
                #pm.api.select('CA_atom1', f'seq1///{ints1[i]}/CA', quiet=1)
                #pm.api.select('CA_atom2', f'seq2///{ints2[i]}/CA', quiet=1)
                #temp = pm.api.distance('_','CA_atom1', 'CA_atom2') # angstrems
                #dists[i] = str(temp)
            
            # Выравнивание CA атомов

            try:
                result = pm.api.pair_fit('CA_atoms1','CA_atoms2', quiet=1)
                for i in range(6):
                    pm.api.select('CA_atom1', f'CA_atoms1///{ints1[i]}/CA', quiet=1)
                    pm.api.select('CA_atom2', f'CA_atoms2///{ints2[i]}/CA', quiet=1)
                    #temp = pm.api.distance('_','CA_atom1', 'CA_atom2') # angstrems
                    temp = pm.api.get_distance('CA_atom1', 'CA_atom2') # angstrems
                    dists[i] = str(temp)
                with open(f'fit_res_{m}.csv', 'a') as file:
                    temp = ','.join(dists)
                    file.writelines(f'{k1},{k2},{result},{temp}\n')
            except:
                problems_CA.append([k1, name1, chain1, k2, name2, chain2])
                pass

            pm.api.delete('all')
        else:
            with open(f'fit_res_{m}.csv', 'a') as file:
                file.writelines(f'{k1},{k2},{0},0,0,0,0,0,0\n')

with open('problems_resi.csv', 'w') as file:
    file.writelines('ID,name,chain\n')
    for i in range(len(problems_resi)):
        file.writelines(
               str(problems_resi[i][0]) + ',' + \
               str(problems_resi[i][1]) + ',' + \
               str(problems_resi[i][2]) + '\n')

with open('problems_CA.csv', 'w') as file:
    file.writelines('ID1,name1,chain1,ID2,name2,chain2\n')
    for i in range(len(problems_CA)):
        file.writelines(
               str(problems_CA[i][0]) + ',' + \
               str(problems_CA[i][1]) + ',' + \
               str(problems_CA[i][2]) + ',' + \
               str(problems_CA[i][3]) + ',' + \
               str(problems_CA[i][4]) + ',' + \
               str(problems_CA[i][5]) + '\n')
