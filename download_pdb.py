import pymol as pm
from os import system

system('mkdir pdb')
pm.cmd.set('fetch_path', pm.cmd.exp_path('./pdb/'), quiet=0)

with open('./names.txt') as file:
    names = file.readlines()

for i in range(len(names)):
    names[i] = names[i].rstrip()

for p in names:
    pm.api.fetch(p)
    pm.api.delete('all')
