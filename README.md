# Сlassification of β-arches based on their 3D structure

Supervisor

- Stanislav Bondarev

Students:

1. Basyrov Rustam
2. Zhozhikov Leonid

## Introduction

β-arches are structural elements of proteins that include two β-strands united by a turn. 
They are present in the proteins of β-solenoids, as well as amyloid aggregates.
In the original article in which the term was proposed, such structures were divided into groups based on the conformation of amino acids at the turn sites, as well as the number of amino acids in them.
As part of the project, we analyzed the diversity of 3D β-arch organizations.

To describe the three-dimensional organization of β-arches for all possible structures, we chose two parameters:

- RMSD 
- Torsion angles

![calc](RMSD_calc.png)
![calc](TA_calc.jpg)

## Aim, tasks and data

The **aim** of the project is classification of β-arches based on their 3D structure.
To achieve it, the following **tasks** were solved:

1. Collected information about β-arches in a single database.
2. A program was written to align  two  β-arches and calculate RMSD between them.
3. Torsion angles were calculated and the possibility of clustering by them was evaluated.
4. Maked clusterization of β-arches based on RMSD.
5. Maked clusterization of β-arches based on torsion angles of AA in β-strands.

The **data** for the project is information about beta-arches 
(there is a [fragment](structs.csv) in the repository).

## Results

First, we calculate RMSD.

![heatmap](heatmap.png)

In the course of the project, we had to filter out some of the beta-arches due to:

1. Problems with the calculation of torsion angles for unknown reasons
2. A small number of representatives of some types of beta-arches

As a result, about 880 structures remained.

Based on RMSD, hierarchical clustering was carried out. Different colors highlight different types of arches, according to the original nomenclature. It can be noted that these data make it possible to distinguish various clusters of structures that are similar to each other, but differ greatly from elements from other groups. Moreover, in general, the groups we discovered are comparable to the original ones, however, there are a number of exceptions when β-arches of the same type break up into separate groups.

![dend](dend.png)

The 10-dimensional space of torsion angles was reduced to 3-dimensional using the principal component analisys. Unlike the previous parameter, this data set does not allow dividing the existing set of β-arches into separate structure groups. We observe one large point cloud and several smaller ones.

![pca](pca.gif)

Finally, we analyzed the differences in torsional angles between different β-arches. Using the pairwise Wilcoxon test, hypotheses about the equality of medians of different angles for different types of β-arcs were tested. The slide shows the phi2 case. Significantly different medians are indicated. Thus, we can conclude that some types of β-arcs differ significantly from each other in conformations of some residues in β-strands.

![boxplot_phi2](boxplot_phi2.jpg)

Below are the boxplots for the rest of the torsion angles.

![boxplots](boxplot.png)

## Conclusion

Based on the results of the work performed, it can be concluded that torsion angles are poorly suitable for refining the current classification, but with the help of RMSD it can be improved and supplemented.

## Project reproducibility

### Requirements

The following `Python` libraries are required:

- RMSD calculation
    - `pymol`
- Torsion angles calculation
    - Jupyter Notebook
    - `pymol`
    - `numpy`
    - `pandas`
- Torsion angles clusterization
    - Jupyter Notebook
    - `sklearn`
    - `pandas`

The following `R` libraries are required:

- Hierarchical clustering (plot dendrogram)
    - `dendextend`
- Drawing boxplots
    - `ggplot2`

### Procedure

1. Download `.cif`-files from PDB

```
python download_pdb.py
```

2. Calculate RMSD

```
python fittingV6.py > /dev/null
```

3. Draw heatmap in [heatmap.ipynb](heatmap.ipynb)
You should get this image

![image](heatmap.example.jpg)

4. Calculate torsion angles in [TorsionAngles.ipynb](TorsionAngles.ipynb)
5. Cluster structs by them and draw PCA in [TorsionAngles.ipynb](TorsionAngles.ipynb)

![gif](pca.example.gif)

6. Cluster structs by RMSD in [hclust.RMSD.R](hclust.RMSD.R)
You should get this image

![image](hclust.example.png)

7. Convert torsion angles for `R` in [TorsionAngles.ipynb](TorsionAngles.ipynb)
8. Draw boxplots and run pairwise Wilcoxon test in [tors_angles.wilcox.R](tors_angles.wilcox.R)
You should get this image

![image](boxplot.example.png)
